SHELL:=/bin/bash
ARCH:=x86_64
DISTRO:=$(shell lsb_release -si | tr '[A-Z]' '[a-z]')-$(shell lsb_release -rs)
PKG:=cmake
COMPILER:=gnu
JOBS:=-j $(shell grep ^siblings /proc/cpuinfo  | head -1 | awk ' { print $$3 } ')
SOURCEFILE:=$(PKG)-$(VER).tar.gz

URL:=https://github.com/Kitware/CMake/releases/download/v$(VER)/cmake-$(VER).tar.gz

INSTALLDIR:=/usr/local/shared/$(DISTRO)/$(ARCH)/$(PKG)/$(VER)/$(COMPILER)
CONFFLAGS:=

ifeq ($(COMPILER),gnu)
# Some packages like these to be explicit
#CONFFLAGS+=CC=gcc CXX=g++ FC=gfortran F77=gfortran
MODULECMD:=true
endif

ifeq ($(COMPILER),intel)
CONFFLAGS:=CC=icx CXX=icpc FC=ifort F77=ifort
MODULECMD:=module purge && module load icc
endif

info:
ifeq ($(VER),)
	@echo To build $(PKG)
	@echo make build VER=X.Y.Z [COMPILER=\(gnu\|intel\)]
	@echo
	@echo To see where the resulting binaries will end up,
	@echo make info VER=X.Y.Z [COMPILER=\(gnu\|intel\)]
	@echo
else
	@echo make build VER=$(VER) COMPILER=$(COMPILER)
	@echo will install into $(INSTALLDIR)
	@echo
endif

clean:
	rm -Rf build libtool.patch

.PHONY: build download

download: $(SOURCEFILE)

$(SOURCEFILE):
	wget -O $(SOURCEFILE) $(URL)

libtool.patch: libtool.patch.raw
	@# Escape ampersands in substitution pattern
	@cat libtool.patch.raw | sed 's/%MODULECMD%/$(subst &,\&,$(MODULECMD))/' >libtool.patch

build: $(SOURCEFILE)
	mkdir -p build && \
	$(MODULECMD) && \
	cd build && \
        tar -xzf ../$(SOURCEFILE) && \
	rm -f src && \
	ln -s * src && \
	cd src && ./configure $(CONFFLAGS) --prefix=$(INSTALLDIR) && \
	make $(JOBS) && \
	sudo mount -o remount,rw /shared/shared && \
	sudo make install
