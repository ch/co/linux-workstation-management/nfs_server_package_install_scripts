SHELL:=/bin/bash
RELEASE_URL:=https://github.com/Unidata/netcdf-fortran/releases

# Sensible defaults, which can be overridden 
ARCH:=x86_64
DISTRO:=$(shell lsb_release -si | tr '[A-Z]' '[a-z]')-$(shell lsb_release -rs)
PKG:=netcdff
COMPILER:=gnu

INSTALLDIR:=/usr/local/shared/$(DISTRO)/$(ARCH)/$(PKG)/$(COMPILER)/$(VERS)
BUILDDIR:=build/$(DISTRO)/$(ARCH)/$(PKG)/$(COMPILER)/$(VERS)

info:
ifeq ($(VERS),)
	@echo Determine potential versions by examining $(RELEASE_URL)
	@echo
	@echo To build $(PKG)
	@echo make build VERS=X.Y.Z [COMPILER=\(gnu\|intel\)]
	@echo
	@echo To see where the resulting binaries will end up,
	@echo make info VERS=X.Y.Z [COMPILER=\(gnu\|intel\)]
	@echo
else
	@echo make build VERS=$(VERS) COMPILER=$(COMPILER)
	@echo will install into $(INSTALLDIR)
	@echo
endif

clean:
	rm -Rf build

v%.tar.gz:
	wget https://github.com/Unidata/netcdf-fortran/archive/refs/tags/v$*.tar.gz

.PRECIOUS: %.tar.gz build/%

FORCE:

.SECONDEXPANSION: 

$(BUILDDIR)/build/configure: v$(VERS).tar.gz
	mkdir -p build/$(DISTRO)/$(ARCH)/$(PKG)/$(COMPILER)/$(VERS)
	zcat v$(VERS).tar.gz | (cd $(BUILDDIR) && tar -xf -) 
	cd $(BUILDDIR) && rm -f build && ln -s * build

# Ensure things we need to specify are specified
ifeq ($(COMPILER),)
install: nocompiler

nocompiler:
	@echo Must specify COMPILER
	@exit 1
endif

ifeq ($(VERS),)
install: novers

novers:
	@echo Must specify VERS
	@exit 1
endif

ifneq ($(VERS),)
ifneq ($(COMPILER),)
install: $(INSTALLDIR)/.
	echo OK
endif
endif

CONFARGS=--disable-dap-remote-tests --enable-netcdf-4
CONFARGS+=--prefix=$(INSTALLDIR)

MODULES=netcdf/$(COMPILER)

ifeq ($(COMPILER),intel)
#MODULES+=icc/64/2022/0/1
MODULES+=icc/64/2022
CONFENV=CC=icc FC=ifort
endif

MODULES+=hdf5/$(COMPILER)

$(INSTALLDIR)/.:
	module purge && module add $(MODULES) &&\
        make build/$(DISTRO)/$(ARCH)/$(PKG)/$(COMPILER)/$(VERS)/build/configure &&\
	cd build/$(DISTRO)/$(ARCH)/$(PKG)/$(COMPILER)/$(VERS)/build &&\
	$(CONFENV) ./configure $(CONFARGS) &&\
	make &&\
	make check &&\
	sudo mount -o remount,rw /shared/shared &&\
	sudo make install &&\
	echo All complete

