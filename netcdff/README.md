Examine available versions by opening https://github.com/Unidata/netcdf-fortran/releases or
by
 make versions
 cat versions

Download source.tar.gz file manually or by
 make vX.Y.Z.tar.gz
(where X.Y.Z is a version listed)

Build and install the netcdff packages for both compilers

make install PKG=netcdff COMPILER=gnu VERS=X.Y.Z
make install PKG=netcdff COMPILER=intel VERS=X.Y.Z

Create the modulefiles for those netcdff packages in

 /usr/local/share/modules/modulefiles/netcdf/gnu/X.Y.Z
 /usr/local/share/modules/modulefiles/netcdf/intel/X.Y.Z

by adding the appropriate symlinks to the appropriate debian packages
