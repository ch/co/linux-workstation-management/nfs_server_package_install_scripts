#!/bin/bash

# This script is to install Intel OneAPI base and HPC kits.  (aka the Intel
# compilers and MKL). To use it you need to have downloaded a matching pair of
# the full offline installer packages for the OneAPI base kit and the HPC kit.
# Place those in your working directory on a managed Linux machine where you
# can get write access to the NFS server (ie you may need to edit /etc/exports
# on moya).

set -x
set -e

INSTALLBASE=/usr/local/shared/intel_oneapi

install() {
    sh ${BASEKIT_FILE} -a --intel-sw-improvement-program-consent=decline --eula=accept --install-dir=${INSTALLROOT} -s
    sh ${HPCKIT_FILE} -a --intel-sw-improvement-program-consent=decline --eula=accept --install-dir=${INSTALLROOT} -s
    make_intel_native_modules
    for f in $INSTALLROOT/compiler/*
    do
      if [ -d "$f" ]; then
        fix_up_paths "$f"
      fi
    done
}

prep() {
    if [ $# -lt 3 ] ; then
        echo Usage: $0 basekitinstaller hpckitinstaller toolkitversion
        echo Toolkit version should be the first three parts of the base kit installer version number
        exit 1
    fi
    remove_intel_db
    WORKDIR=$(mktemp -d)
    BASEKIT_FILE=$1
    HPCKIT_FILE=$2
    if [ -f $BASEKIT ] && [ -f $HPCKIT ]
    then
        echo Using base kit $BASEKIT_FILE and HPC kit $HPCKIT_FILE
    else
        echo Can\'t find $BASEKIT_FILE AND $HPCKIT_FILE
        exit 1
    fi
    cd $WORKDIR
    echo $WORKDIR
}

get_version() {
    echo $3
}


fix_up_paths() {
    echo fixing paths
    BASEDIR=$1/linux
    # set rpath in the config file
    for dir in intel64 ia32
    do
        for file in ifort.cfg ifc.cfg icc.cfg icpc.cfg
        do
            if [ -f ${BASEDIR}/bin/${dir}/${file} ] && ! [ -f ${BASEDIR}/bin/${dir}/${file}.saved ]
            then
                cp ${BASEDIR}/bin/${dir}/${file} ${BASEDIR}/bin/${dir}/${file}.saved
                # check if the file already contains extra flags
                if egrep -q '^[^ #]' ${BASEDIR}/bin/${dir}/${file}
                then
                    sed -e "/^[^ #]/ s@^@-Xlinker -rpath -Xlinker ${BASEDIR}/lib:${BASEDIR}/lib/x64:${BASEDIR}/lib/emu:${BASEDIR}/compiler/lib/intel64_lin -I $BASEDIR/include @" < ${BASEDIR}/bin/${dir}/$file > ${BASEDIR}/bin/${dir}/$file.new
                else
                    echo "-Xlinker -rpath -Xlinker ${BASEDIR}/lib:${BASEDIR}/lib/x64:${BASEDIR}/lib/emu:${BASEDIR}/compiler/lib/intel64_lin -I $BASEDIR/include" > ${BASEDIR}/bin/${dir}/$file.new
                fi
                mv ${BASEDIR}/bin/${dir}/$file.new ${BASEDIR}/bin/${dir}/$file
                chmod a+x ${BASEDIR}/bin/${dir}/$file
            fi
        done
    done

    # fix up the drivers
    for dir in intel64
    do
        for file in icc ifort icpc ifc
        do
            fullpath=${BASEDIR}/bin/${dir}/${file}
            #echo $fullpath
            if [ -f ${fullpath} ] && ! [ -f ${fullpath}.bin ]
            then
                cp ${fullpath} ${fullpath}.bin
                cat <<EOF > ${fullpath}
#!/bin/bash
exec -a \$0 ${fullpath}.bin \${LIBRARY_PATH:+-Xlinker -rpath -Xlinker \$LIBRARY_PATH} "\$@"
EOF

                chmod a+x ${fullpath}
            fi
        done
    done

}

clean() {
    cd 
    [ -d $WORKDIR ] && rm -rf $WORKDIR
    remove_intel_db
    remove_dotscripts
    readable_files
}

remove_intel_db() {
    rm -f /opt/intel/intel_sdp_products.db
    rm -rf /var/intel
}

remove_dotscripts() {
    rm -rf $INSTALLROOT/.scripts
}

readable_files() {
    chmod -R o+rX $INSTALLROOT
}

mount_rw() {
    mount /shared/shared -o remount,rw
}

make_intel_native_modules() {
	${INSTALLROOT}/modulefiles-setup.sh --force
}

##############################################################################

DISTS="focal"

prep $@ # sets WORKDIR, BASEKIT_FILE and HPCKIT_FILE
VERSION=$3
echo "VERSION is $VERSION"
INSTALLROOT=${INSTALLBASE}/${VERSION}
mount_rw

install

clean

echo "ALL DONE"
