Examine available versions by opening https://github.com/Unidata/netcdf-c/releases

make install COMPILER=gnu VERS=X.Y.Z
make install COMPILER=intel VERS=X.Y.Z

Create the modulefiles for those netcdf packages in

 /usr/local/share/modules/modulefiles/netcdf/gnu/X.Y.Z
 /usr/local/share/modules/modulefiles/netcdf/intel/X.Y.Z

