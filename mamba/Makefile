SHELL:=/bin/bash
ARCH:=x86_64
DISTRO:=$(shell lsb_release -si | tr '[A-Z]' '[a-z]')-$(shell lsb_release -rs)
PKG:=mamba
#COMPILER:=gnu
JOBS:=-j $(shell grep ^siblings /proc/cpuinfo  | head -1 | awk ' { print $$3 } ')
SOURCEFILE:=Miniforge3-Linux-$(ARCH).sh

URL:=https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-Linux-$(ARCH).sh

INSTALLDIR:=/usr/local/shared/mamba/python3/$(VER)
BUILDDIR=build/$(DISTRO)/$(ARCH)/$(PKG)/$(VER)/$(COMPILER)
RIDDLIUB=../../../../../..
CONFFLAGS:=--with-wrapper-ldflags="-Wl,-rpath -Wl,$(INSTALLDIR)/lib"

ifeq ($(COMPILER),gnu)
# Some packages like these to be explicit
#CONFFLAGS+=CC=gcc CXX=g++ FC=gfortran F77=gfortran
CONFFLAGS+=FFLAGS=-fallow-argument-mismatch FCFLAGS=-fallow-argument-mismatch
MODULECMD:=true
PATCHCMD:=true
endif

ifeq ($(COMPILER),intel)
CONFFLAGS:=CC=icx CXX=icpc FC=ifort F77=ifort
MODULECMD:=module purge && module load icc
PATCHCMD:=patch -p2 --ignore-whitespace <libtool.patch
endif

info:
ifeq ($(VER),)
	@echo To build $(PKG)
	@echo make build VER=X.Y.Z [COMPILER=\(gnu\|intel\)]
	@echo
	@echo To see where the resulting binaries will end up,
	@echo make info VER=X.Y.Z [COMPILER=\(gnu\|intel\)]
	@echo
else
	@echo 'make build VER=$(VER) COMPILER=$(COMPILER)'
	@echo 'will compile in $(BUILDDIR) via'
ifneq ($(MODULECMD),true)
	@echo '$(MODULECMD)'
endif
	@echo './configure  $(CONFFLAGS) --prefix=$(INSTALLDIR)'
	@echo 'and install into $(INSTALLDIR)'
	@echo
endif

# If both VER and COMPILER are specified, prune gently
clean:
ifneq ($(VER),)
ifneq ($(COMPILER),)
	rm -Rf $(BUILDDIR)
endif
endif
ifeq ($(VER),)
	rm -Rf build libtool.patch
endif
ifeq ($(COMPILER),)
	rm -Rf build libtool.patch
endif

.PHONY: build download

download: $(SOURCEFILE)

$(SOURCEFILE):
	wget -O $(SOURCEFILE) $(URL)

.SECONDEXPANSION:

# Unpack the source file, and create a stampfile (don't use a file from the .tar - timestamps therein 

build: 
ifeq ($(VER),)
	@echo make build VER=VERSION
else
	sudo mount -o remount,rw /shared/shared && \
	sudo bash $(SOURCEFILE) -p $(INSTALLDIR)
	sudo rm -rf $(INSTALLDIR)/pkgs/cache
endif
